package com.epam.rd.testing.utils;

import com.epam.rd.userapi.pojo.UserResponse;

import java.util.ArrayList;

public class UserTestsUtils {

    public static ArrayList<UserResponse> fillUsersCollection(int numberOfElements) {
        ArrayList<UserResponse> usersCollection = new ArrayList<>();
        for (int i = 0; i < numberOfElements; i++) {
            UserResponse user = new UserResponse();
            user.setUserId(Long.valueOf(i));
            user.setName("UserName_" + i);
            usersCollection.add(user);
        }
        return usersCollection;
    }
}