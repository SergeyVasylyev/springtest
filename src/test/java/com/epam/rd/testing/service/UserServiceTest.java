package com.epam.rd.testing.service;

import com.epam.rd.testing.utils.UserTestsUtils;
import com.epam.rd.userapi.pojo.UserResponse;
import com.epam.rd.userapi.service.UserService;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.Collection;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.core.Is.is;

public class UserServiceTest {

    private final RestTemplate mockRestTemplate = Mockito.mock(RestTemplate.class);
    private final UserService userService = new UserService(mockRestTemplate);

    @Test
    public void getAllUsersCorrectSizeMatchesAllValues() {
        //GIVEN
        int numberOfElements = 5;
        List<UserResponse> usersCollection = UserTestsUtils.fillUsersCollection(numberOfElements);
        ResponseEntity<List<UserResponse>> userResponseList = new ResponseEntity<>(usersCollection, HttpStatus.OK);

        Mockito.when(mockRestTemplate.exchange(
                ArgumentMatchers.any(URI.class),
                ArgumentMatchers.eq(HttpMethod.GET),
                ArgumentMatchers.<HttpEntity<List<UserResponse>>>any(),
                ArgumentMatchers.<ParameterizedTypeReference<List<UserResponse>>>any())
        ).thenReturn(userResponseList);

        //WHEN
        Collection<UserResponse> resultUsersCollection = userService.getAllUsers();

        //THEN
        assertThat(resultUsersCollection, hasSize(numberOfElements));
        usersCollection.forEach(
                userResponse -> assertThat(resultUsersCollection, hasItem(allOf(
                        hasProperty("userId", is(userResponse.getUserId())),
                        hasProperty("userName", is(userResponse.getUserName()))
                )))
        );
    }

    @Test
    public void getAllUsersReturnEmptyCollectionWhenThrowsException() {
        //GIVEN
        Mockito.when(mockRestTemplate.exchange(
                ArgumentMatchers.any(URI.class),
                ArgumentMatchers.eq(HttpMethod.GET),
                ArgumentMatchers.<HttpEntity<List<UserResponse>>>any(),
                ArgumentMatchers.<ParameterizedTypeReference<List<UserResponse>>>any())
        ).thenThrow(new RestClientException("mocked for test RestClientException"));

        //WHEN
        Collection<UserResponse> resultUsersCollection = userService.getAllUsers();

        //THEN
        assertThat(resultUsersCollection, hasSize(0));
    }
}
