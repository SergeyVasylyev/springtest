package com.epam.rd.testing.controller;

import com.epam.rd.testing.utils.UserTestsUtils;
import com.epam.rd.userapi.controller.UserController;
import com.epam.rd.userapi.pojo.UserResponse;
import com.epam.rd.userapi.service.UserService;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.Collection;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class UserControllerTest {

    private final UserService mockUserService = Mockito.mock(UserService.class);
    private final UserController userController = new UserController(mockUserService);
    private final MockMvc mockMvc = MockMvcBuilders.standaloneSetup(userController).build();

    @Test
    public void getAllUsersCalledFromService() throws Exception {
        //GIVEN
        Collection<UserResponse> usersCollection = UserTestsUtils.fillUsersCollection(5);
        Mockito.when(mockUserService.getAllUsers()).thenReturn(usersCollection);

        //WHEN
        mockMvc.perform(get("/users"));

        //THEN
        Mockito.verify(mockUserService).getAllUsers();
    }

    @Test
    public void getAllUsersReturnCorrectSizeAndData() throws Exception {
        //GIVEN
        int numberOfElements = 6;
        ArrayList<UserResponse> usersCollection = UserTestsUtils.fillUsersCollection(numberOfElements);
        Mockito.when(mockUserService.getAllUsers()).thenReturn(usersCollection);

        //WHEN
        ResultActions perform = mockMvc.perform(get("/users").accept(MediaType.APPLICATION_JSON_VALUE));

        //THEN
        perform.andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(numberOfElements)))
                .andExpect(jsonPath("$[0].id", Matchers.is(usersCollection.get(0).getUserId().intValue())))
                .andExpect(jsonPath("$[" + (numberOfElements - 1) + "].id", Matchers.is(usersCollection.get(numberOfElements - 1).getUserId().intValue())))
                .andExpect(jsonPath("$[0].username", Matchers.equalTo(usersCollection.get(0).getUserName())))
                .andExpect(jsonPath("$[" + (numberOfElements - 1) + "].username", Matchers.equalTo(usersCollection.get(numberOfElements - 1).getUserName())));
    }
}
