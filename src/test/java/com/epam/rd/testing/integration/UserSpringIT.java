package com.epam.rd.testing.integration;

import com.epam.rd.testing.BaseSpringIntegrationConfig;
import com.epam.rd.testing.utils.UserTestsUtils;
import com.epam.rd.userapi.pojo.UserResponse;
import lombok.extern.slf4j.Slf4j;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.web.client.RestClientException;

import java.io.IOException;
import java.net.URI;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Slf4j
public class UserSpringIT extends BaseSpringIntegrationConfig {

    @Test
    public void userResponseWorksThroughAllLayersWhenExceptionIsThrown() throws Exception {
        //GIVEN
        Mockito.when(restTemplate.exchange(
                ArgumentMatchers.any(URI.class),
                ArgumentMatchers.eq(HttpMethod.GET),
                ArgumentMatchers.<HttpEntity<List<UserResponse>>>any(),
                ArgumentMatchers.<ParameterizedTypeReference<List<UserResponse>>>any())
        ).thenThrow(new RestClientException("mocked for test RestClientException"));

        //WHEN
        ResultActions perform = mockMvc.perform(get("/users").contentType("application/json"));

        //THEN
        perform.andExpect(status().isOk()).andExpect(jsonPath("$", hasSize(0)));
    }

    @Test
    public void userResponseWorksThroughAllLayers() throws Exception {
        //GIVEN
        List<UserResponse> usersCollection = readFromJson();
        ResponseEntity<List<UserResponse>> userResponseList = new ResponseEntity<>(usersCollection, HttpStatus.OK);
        int numberOfElements = usersCollection.size();

        Mockito.when(restTemplate.exchange(
                ArgumentMatchers.any(URI.class),
                ArgumentMatchers.eq(HttpMethod.GET),
                ArgumentMatchers.<HttpEntity<List<UserResponse>>>any(),
                ArgumentMatchers.<ParameterizedTypeReference<List<UserResponse>>>any())
        ).thenReturn(userResponseList);

        //WHEN
        ResultActions perform = mockMvc.perform(get("/users").contentType("application/json"));

        //THEN
        perform.andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(numberOfElements)))
                .andExpect(jsonPath("$[0].id", Matchers.is(usersCollection.get(0).getUserId().intValue())))
                .andExpect(jsonPath("$[" + (numberOfElements - 1) + "].id", Matchers.is(usersCollection.get(numberOfElements - 1).getUserId().intValue())))
                .andExpect(jsonPath("$[0].username", Matchers.equalTo(usersCollection.get(0).getUserName())))
                .andExpect(jsonPath("$[" + (numberOfElements - 1) + "].username", Matchers.equalTo(usersCollection.get(numberOfElements - 1).getUserName())))
                .andExpect(jsonPath("$[0].company.name", Matchers.equalTo(usersCollection.get(0).getCompany().getName())))
                .andExpect(jsonPath("$[" + (numberOfElements - 1) + "].company.name", Matchers.equalTo(usersCollection.get(numberOfElements - 1).getCompany().getName())));
    }

    private List<UserResponse> readFromJson() {
        ClassPathResource classPathResource = new ClassPathResource("/responses/jsonplaceholder_user_response.json");
        try {
            return objectMapper.readValue(classPathResource.getInputStream(), objectMapper.getTypeFactory().constructCollectionType(List.class, UserResponse.class));
        } catch (IOException e) {
            log.error(String.format("Cannot read from file: %s", e.getMessage()), e);
        }

        return null;
    }
}